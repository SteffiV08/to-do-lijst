<?php
require_once('bootstrap.php');


if (isset($_GET['id'])) {
    $taak = $query->selectwithid('taken', $_GET['id']);
}


if (isset($_POST['verzend'])) {
    $query->insert('taken', [
        'titel' => $_POST['titel'],
        'omschrijving' => $_POST['omschrijving'],
        'datum' => $_POST['datum'],
    ]);
}
?>
<div class="mx-auto w-1/3">

    <h1 class="mb-8 mt-8 text-center">Voeg nieuwe taak toe</h1>
    <p class="mb-4 mt-2 overflow-hidden">
        <a href="index.php" class="no-underline float-right">
            <button class="bg-grey hover:bg-grey-dark hover:text-white text-grey-darkest font-bold py-2 px-4 rounded block">
                Ga terug
            </button>
        </a>
    </p>
<?php

require_once('views/partials/form.view.php');
require_once('views/partials/end.php');
?>
</div>