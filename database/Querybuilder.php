<?php

class Querybuilder
{
    public $pdo;

    //$pdo komt binnen via bootstrap.php
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    //alle taken ophalen
    public function selectAll($table)
    {
        $classname = substr(ucfirst($table), 0, -1);
        $stmt = $this->pdo->prepare("select * from {$table}");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, $classname);
    }

    //1taak ophalen
    public function selectwithid($table, $id)
    {
        $classname = substr(ucfirst($table), 0, -1);
        $stmt = $this->pdo->prepare("select * from {$table} where id=$id");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, $classname)[0];
    }

    //toevoegen aan databank
    public function insert($table, $parameters)
    {
        $sql = sprintf('insert into %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($parameters);
            header('Location: index.php');
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    //verwijderen uit databank
    public function deletewithid($table, $id)
    {
        $stmt = $this->pdo->prepare("delete from {$table} where id={$id}");
        $stmt->execute();
        header('Location: index.php');
    }

    //bewerk
    public function update($table, $parameters, $id)
    {

        foreach (array_keys($parameters) as $key) {
            $para[] = $key . '=:' . $key;
        }

        $sql = sprintf('update %s set %s where id=%s',
            $table,
            implode(',', $para),
            $id
        );


        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($parameters);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    //change status
    public function changeboolean($table, $status, $id)
    {
        /*UPDATE dbo.Table1 SET col2 = CASE WHEN col2=1 THEN 0 ELSE 1 END);*/
        $sql = sprintf('update %s set %s = case when %s= 1 then 0 else 1 end where id=%s',
            $table,
            $status,
            $status,
            $id
        );

        // dd($sql);
        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array($status));
            header('Location: index.php');

        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}