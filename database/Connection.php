<?php

class Connection
{
    //STATIC FUNCTION: je moet geen object aanmaken om gebruik te maken van deze functie
    public static function make()
    {
        //try maak en return pdo object and catch -> probeer, indien niet gelukt -> catch
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=todo', 'root', 'root');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
