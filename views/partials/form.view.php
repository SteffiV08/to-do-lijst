<?php

if (isset($_GET['id'])) {
    $taak = $query->selectwithid('taken', $_GET['id']);
}


?>

<form method="post">
    <input type="hidden" name="id" value="<?= $taak->ID ?>">
    <label for="titel" class="block mb-2">Titel</label>
    <input type="text" id="titel" name="titel" value="<?= $taak->titel ?? "" ?>" class="border border-grey p-2 block mb-3">
    <label for="omschrijving" class="block mb-2">Omschrijving</label>
    <textarea name="omschrijving" id="omschrijving" cols="30" rows="6" class="border border-grey p-2 block mb-3"><?= $taak->omschrijving ?? "" ?></textarea>
    <label for="datum" class="block mb-2">Tegen</label>
    <input type="date" id="datum" name="datum" value="<?= $taak->datum ?? "" ?>" class="border border-grey p-2 block mb-3">
    <label for="status" class="mb-3">Al gedaan </label>
    <input type="checkbox" value="1" name="status" id="status" <?= ($taak->status ?? '') ? 'checked' : '' ?>>
    <input type="submit" name="verzend" class="bg-blue hover:bg-blue-dark text-white font-semibold py-2 px-2 border border-blue rounded block mt-3">
</form>