<p class="-mt-8 mb-8 mr-4 text-center"><a href="taak_nieuw.php">
        <button class="bg-blue hover:bg-blue-dark text-white font-semibold py-8 px-8 border border-blue rounded">
            Voeg nieuwe taak toe
        </button>
    </a></p>
<div class="flex overflow-hidden flex-wrap  justify-between">
    <?php

    $taken = $query->selectAll('taken');
    foreach ($taken as $taak) {

        ?>

        <div class="w-1/4 rounded border border-grey p-6 mb-8 mr-4">
            <p class="<?php if ($taak->status == 1) {
                echo 'bg-green-lighter';
            } else {
                echo 'bg-red';
            } ?> pt-1 pb-1 mb-1 rounded text-center">
                <?php if ($taak->status == 0) { ?>
                    Nog te doen
                    <?php
                } else { ?>
                    Gedaan
                <?php }; ?>
            </p>
            <div class="<?php if ($taak->status == 1) {
                echo 'line-through';
            } else {
                echo '';
            } ?> mb-1">

                <p><b>Titel:</b> <?= $taak->titel; ?></p>
                <p><b>Omschrijving:</b> <?= $taak->omschrijving; ?></p>
                <p><b>Tegen:</b> <?= $taak->datum; ?></p>


                <p>
                    <?php
                    //Dag van vandaag
                    $today = date_create(date('Y-m-d'));

                    //Dag van de taak
                    $event = date_create($taak->datum);

                    //bereken verschil
                    $countdown = date_diff($today, $event);
                    //dd($countdown);

                    //laten zien
                    echo "Je hebt nog <b>" . $countdown->format("%R%a") . "</b> dagen de tijd";
                    ?>
                </p>
            </div>
            <div class="inline-flex mt-1">
                <p><a href="taak_bewerk.php?id=<?= $taak->ID ?>">
                        <button class="bg-transparent hover:bg-orange text-orange-dark font-semibold hover:text-white py-2 px-2 mr-2 border border-orange hover:border-transparent rounded">
                            Bewerk
                        </button>
                    </a></p>
                <p><a href="index.php?delete=<?= $taak->ID ?>">
                        <button class="bg-transparent hover:bg-red text-red-dark font-semibold hover:text-white py-2 px-2 mr-2 border border-red hover:border-transparent rounded">
                            Verwijder
                        </button>
                    </a></p>
                <p><a href="index.php?status=<?= $taak->ID ?>">
                        <button class="bg-transparent hover:bg-grey-darkest text-grey-dark-darkest font-semibold hover:text-white py-2 px-2 border border-grey-darkest hover:border-transparent rounded">
                            Status
                        </button>
                    </a></p>
            </div>
        </div>

    <?php } ?>
</div>