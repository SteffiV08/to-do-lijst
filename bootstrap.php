<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once('database/Connection.php');
require_once('inc/functions.inc.php');
require_once('database/Querybuilder.php');
require_once('classes/Taken.php');
require_once('views/partials/start.php');

$pdo = Connection::make();
$query = new Querybuilder($pdo);