<?php

require_once('bootstrap.php');

if (isset($_GET['delete'])) {
    $query->deletewithid('taken', $_GET['delete']);
}

if (isset($_GET['status'])) {
    $query->changeboolean('taken', 'status', $_GET['status']);
}

require_once ("views/index.view.php");
require_once("views/partials/end.php");